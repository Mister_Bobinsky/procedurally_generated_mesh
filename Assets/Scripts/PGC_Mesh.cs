﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PGC_Mesh : MonoBehaviour
{
    public Vector3[] newVertices;
    public Vector2[] newUV;
    public int[] newTriangles;

    public Vector3[] newVerts2;
    public Vector2[] newUV2;
    public int[] newTriangles2;

    Mesh mesh;

    MeshRenderer meshMaterial;

    public int meshSize;
    public int xSize;
    public int ySize;

    public void Start()
    {
        /*
         * Creates Mesh / MeshFilter
         */
        mesh = new Mesh();
        gameObject.AddComponent<MeshFilter>();
        GetComponent<MeshFilter>().mesh = mesh;
        gameObject.AddComponent<MeshCollider>();

        /*
         * 'The Image / Material Addition'
         *  - Creates a MeshRenderer and loads a specified meshMaterial 
         *    from the Resources folder in the Unity project: Loads in
         *    a material named "MeshMaterial" that displays the image
         *    on the mesh.
         */
        gameObject.AddComponent<MeshRenderer>();
        meshMaterial = gameObject.GetComponent<MeshRenderer>();
        meshSize = xSize * ySize;
        meshMaterial.material = Resources.Load<Material>("MeshMaterial");

        newVerts2 = new Vector3[(xSize + 1) * (ySize + 1)];
        newTriangles2 = new int[xSize * ySize * 2];
        newUV2 = new Vector2[(xSize + 1) * (ySize + 1)];

        /*
         * Cube Creation
         */
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.position = new Vector3(7.0f, 5.0f, 3.0f);
        cube.AddComponent<Rigidbody>();
        //cube.AddComponent<MeshCollider>();

        buildMesh();
        Update();
    }

    void buildMesh()
    {
        /*
         * 'The Vertice Function'
         *  - Maps out the vertices for the triangles to be created with
         *    by using the x and y sizes defined during Unity runtime.
         */
        int i = 0;
        for(int y = 0; y <= ySize; y++)
        {
            for(int x = 0; x <= xSize; x++)
            {
                newUV2[i] = new Vector2((float)x / (float)xSize, (float)y / (float)ySize);
                newVerts2[i++] = new Vector3(x, y, 1);
                
            }
        }

        /*
         * Texture Coordinates
         */
        newUV = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(1,0),
            new Vector2(0,1),
            new Vector2(1,1)
        };
        
        /*
         * 'The Triangle Function'
         *  - Links the vertices using a clockwise ordering to create 
         *    a full mesh at the specified meshSize, xSize, & ySize.
         */
        newTriangles2 = new int[newVerts2.Length * 6];
        i = 0;
        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                newTriangles2[i++] = y * (xSize + 1) + x;
                newTriangles2[i++] = (y + 1) * (xSize + 1) + x;
                newTriangles2[i++] = y * (xSize + 1) + (x + 1);
                newTriangles2[i++] = y * (xSize + 1) + (x + 1);
                newTriangles2[i++] = (y + 1) * (xSize + 1) + x;
                newTriangles2[i++] = (y + 1) * (xSize + 1) + (x + 1);
            }
        }
    }

    void Update()
    {   
        /*
         * 'The Wiggle Function'
         *  - Uses sine functions to constantly update the vertices of the mesh
         *    and create the wave like motion.
         */
        int i = 0;
        for(i = 0; i < newVerts2.Length; i++)
        {
            newVerts2[i].z = Mathf.Sin(Time.time + i);
        }

        mesh.Clear();
        mesh.vertices = newVerts2;
        mesh.triangles = newTriangles2;
        mesh.uv = newUV2;
        mesh.RecalculateNormals();
        mesh.Optimize();

        /*
         * Mesh Collision Detection
         */
        GetComponent<MeshCollider>().sharedMesh = null;
        GetComponent<MeshCollider>().sharedMesh = GetComponent<MeshFilter>().sharedMesh;

        /*
         * - Proposed Solution to 'Non-convex MeshCollider with non-kinematic Rigidbody.'
         * 
         *   transform.GetComponent<MeshCollider>().convex = true;
         * 
         * - Doesn't get rid of error & wrecks the collision between the cube and mesh.
         */
    }
}
