Authors: **Roman Mattia**

RedID: 820771491

Rijeka GitLab: https://rijeka.sdsu.edu/Roman_Mattia/cs596s2020_mattia_roman_proj_00

**Works Cited:**

- https://docs.unity3d.com/ScriptReference/Mesh.html
- https://docs.unity3d.com/ScriptReference/Mesh-vertices.html
- https://docs.unity3d.com/ScriptReference/Mesh-triangles.html
- https://docs.unity3d.com/Manual/Example-CreatingaBillboardPlane.html
- 'Various Google Images'